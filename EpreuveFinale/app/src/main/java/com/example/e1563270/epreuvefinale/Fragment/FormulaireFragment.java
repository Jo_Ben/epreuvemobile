package com.example.e1563270.epreuvefinale.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.e1563270.epreuvefinale.DAL.ChampCompetence;
import com.example.e1563270.epreuvefinale.DAL.Cours;
import com.example.e1563270.epreuvefinale.FormulaireActivity;
import com.example.e1563270.epreuvefinale.Network.Api;
import com.example.e1563270.epreuvefinale.Network.ApiChampCompetence;
import com.example.e1563270.epreuvefinale.Network.ApiCours;
import com.example.e1563270.epreuvefinale.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FormulaireFragment extends Fragment {

    private View view;
    private int idCours;
    private EditText editText;
    private List<ChampCompetence> champCompetenceList;
    private Spinner spinner;
    private Button sauvegarde;
    private String sigle;
    private String titre;
    private int duree;
    private String description;
    private String ponderation;
    private String contenu;
    private String epreuveFinale;
    private ChampCompetence cc;
    private Cours cours;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_formulaire,container,false);
        idCours = getActivity().getIntent().getIntExtra("idCours", -1);
        getCoursParId(idCours);
        getChampsCompetences(idCours);
        setButtonListener(idCours);
        return view;
    }

    public void getCoursParId(int idCours) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Call<Cours> call = ApiCours.getCoursParId(idCours);

        call.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(Call<Cours> call, Response<Cours> response) {
                cours = response.body();
                setUpUI();
            }
            @Override
            public void onFailure(Call<Cours> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setUpUI(){
        editText = view.findViewById(R.id.sigle);
        editText.setText(cours.getSigle());

        editText = view.findViewById(R.id.titre);
        editText.setText(cours.getTitre());

        editText = view.findViewById(R.id.duree);
        editText.setText(String.valueOf(cours.getDuree()));

        editText = view.findViewById(R.id.ponderation);
        editText.setText(cours.getPonderation());

        editText = view.findViewById(R.id.description);
        editText.setText(cours.getDescription());

        editText = view.findViewById(R.id.contenu);
        editText.setText(cours.getContenu());

        editText = view.findViewById(R.id.epreuvefinale);
        editText.setText(cours.getEpreuveFinale());
    }

    public void getChampsCompetences(int idCours) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiChampCompetence ApiChampCompetence = retrofit.create(ApiChampCompetence.class);

        Call<List<ChampCompetence>> call = ApiChampCompetence.getChampsCompetencesPourCours(idCours);

        call.enqueue(new Callback<List<ChampCompetence>>() {
            @Override
            public void onResponse(Call<List<ChampCompetence>> call, Response<List<ChampCompetence>> response) {
                champCompetenceList = response.body();
                SetUpSpinner();
            }
            @Override
            public void onFailure(Call<List<ChampCompetence>> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void SetUpSpinner(){
        spinner = view.findViewById(R.id.spinnerChampsCompetences);
        ArrayAdapter<ChampCompetence> adapter = new ArrayAdapter<ChampCompetence>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item , champCompetenceList );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void setButtonListener(final int idCours) {
        sauvegarde =   view.findViewById(R.id.buttonPutCours);
        sauvegarde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modification();
                PutDonnee(idCours);
            }
        });
    }

    private void Modification() {
        sigle = String.valueOf(((EditText)view.findViewById(R.id.sigle)).getText());
        titre = String.valueOf(((EditText)view.findViewById(R.id.titre)).getText());
        duree = Integer.parseInt(String.valueOf(((EditText)view.findViewById(R.id.duree)).getText()));
        description = String.valueOf(((EditText)view.findViewById(R.id.description)).getText());
        ponderation = String.valueOf(((EditText)view.findViewById(R.id.ponderation)).getText());
        contenu = String.valueOf(((EditText)view.findViewById(R.id.contenu)).getText());
        epreuveFinale = String.valueOf(((EditText)view.findViewById(R.id.epreuvefinale)).getText());
        cc = (ChampCompetence) ((Spinner)view.findViewById(R.id.spinnerChampsCompetences)).getSelectedItem();
        cours = new Cours(idCours, sigle, titre, description, duree, contenu, ponderation, epreuveFinale, cc.getId());
    }

    private void PutDonnee(int idCours) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Call<Cours> call = ApiCours.putCours(idCours,cours);
        call.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(Call<Cours> call, Response<Cours> response) {
                Cours cours = response.body();
                getActivity().onBackPressed();
            }
            @Override
            public void onFailure(Call<Cours> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
