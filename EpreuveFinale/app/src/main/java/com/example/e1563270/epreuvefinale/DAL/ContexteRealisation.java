package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

public class ContexteRealisation {
    @SerializedName("Id")
    private int id;
    @SerializedName("No")
    private int no;
    @SerializedName("Contexte")
    private String contexte;

    @SerializedName("IdCompetence")
    private int idCompetence;
    @SerializedName("Competence")
    private Competence competence;

    public ContexteRealisation() {}

    public ContexteRealisation(int id, int no, String contexte, int idCompetence) {
        this.id = id;
        this.no = no;
        this.contexte = contexte;
        this.idCompetence = idCompetence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getContexte() {
        return contexte;
    }

    public void setContexte(String contexte) {
        this.contexte = contexte;
    }

    public int getIdCompetence() {
        return idCompetence;
    }

    public void setIdCompetence(int idCompetence) {
        this.idCompetence = idCompetence;
    }

    public Competence getCompetence() {
        return competence;
    }

    public void setCompetence(Competence competence) {
        this.competence = competence;
    }

    @Override
    public boolean equals(Object o) {
        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                ContexteRealisation c = (ContexteRealisation) o;
                egale = (id == c.id);
            }
        }
        return egale;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
