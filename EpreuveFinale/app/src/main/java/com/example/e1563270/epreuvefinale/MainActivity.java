package com.example.e1563270.epreuvefinale;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.e1563270.epreuvefinale.Adapters.ListViewAdapter;
import com.example.e1563270.epreuvefinale.DAL.Cours;
import com.example.e1563270.epreuvefinale.DAL.Profil;
import com.example.e1563270.epreuvefinale.Network.Api;
import com.example.e1563270.epreuvefinale.Network.ApiCours;
import com.example.e1563270.epreuvefinale.Network.ApiProfil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<Cours> listCours;
    private Spinner spinner;
    private List<Profil> listProfils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpUI();
        getProfils();
    }

    private void setUpUI(){
        spinner = findViewById(R.id.spinnerprofils);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getCours(listProfils.get(position).getId());
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) { }
        });

        listView = findViewById(R.id.cours_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, FormulaireActivity.class);
                int idCours = listCours.get(position).getId();
                intent.putExtra("idCours", idCours);
                startActivity(intent);
            }
        });
    }

    private void getCours(int idProfil){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Call<List<Cours>> call = ApiCours.getCoursParIdProfils(idProfil);

        call.enqueue(new Callback<List<Cours>>() {
            @Override
            public void onResponse(@NonNull Call<List<Cours>> call, @NonNull Response<List<Cours>> response) {
                List<Cours> listCours = response.body();
                afficherCours(listCours);
            }
            @Override
            public void onFailure(Call<List<Cours>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void afficherCours(List<Cours> cours){
        listCours = cours;
        ListViewAdapter adapter = new ListViewAdapter(this, R.layout.activity_listview, listCours);
        listView.setAdapter(adapter);
    }

    private void getProfils(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiProfil ApiProfil = retrofit.create(ApiProfil.class);

        Call<List<Profil>> call = ApiProfil.getProfils();

        call.enqueue(new Callback<List<Profil>>() {
            @Override
            public void onResponse(@NonNull Call<List<Profil>> call, @NonNull Response<List<Profil>> response) {
                List<Profil> listProfils = response.body();
                afficherProfilSpinner(listProfils);
            }

            @Override
            public void onFailure(Call<List<Profil>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void afficherProfilSpinner(List<Profil> profils){
        listProfils = profils;
        List<String> items = new ArrayList<String>();

        for (Profil p: listProfils)
            items.add(p.getNom());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, items);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }
}
