package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

public class Savoir {
    @SerializedName("Id")
    private int id;
    @SerializedName("No")
    private int no;
    @SerializedName("Description")
    private String description;
    @SerializedName("Type")
    private int type;

    @SerializedName("IdElementCompetence")
    private int idElementCompetence;
    private ElementCompetence elementCompetence;

    public Savoir() {}

    public Savoir(int id, int no, int type, String description, int idElementCompetence) {
        this.id = id;
        this.no = no;
        this.type = type;
        this.description = description;
        this.idElementCompetence = idElementCompetence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return this.no;
    }

    public void setNo(int no) { this.no = no; }

    public int getType(){ return this.type; }

    public void setType(int type) { this.type = type; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdElementCompetence() {
        return idElementCompetence;
    }

    public void setIdElementCompetence(int idElementCompetence) {
        this.idElementCompetence = idElementCompetence;
    }

    public ElementCompetence getElementCompetence() {
        return elementCompetence;
    }

    public void setElementCompetence(ElementCompetence elementCompetence) {
        this.elementCompetence = elementCompetence;
    }

    @Override
    public boolean equals(Object o) {
        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                Savoir c = (Savoir) o;
                egale = (id == c.id);
            }
        }
        return egale;
    }
    @Override
    public int hashCode() {
        return id;
    }
}
