package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Cours {
    @SerializedName("Id")
    private int id;
    @SerializedName("Sigle")
    private String sigle;
    @SerializedName("Titre")
    private String titre;
    @SerializedName("Description")
    private String description;
    @SerializedName("Duree")
    private int duree;
    @SerializedName("Contenu")
    private String contenu;
    @SerializedName("Ponderation")
    private String ponderation;
    @SerializedName("EpreuveFinale")
    private String epreuveFinale;
    @SerializedName("IdChampCompetences")
    private int idChampCompetences;

    private ChampCompetence champCompetence;

    private List<CoursProfil> coursProfils = new ArrayList<>();
    private List<Competence> competence = new ArrayList<>();

    public Cours() {}

    public Cours(int id, String sigle, String titre, String description, int duree, String contenu, String ponderation, String epreuveFinale, int idChampCompetences) {
        this.id = id;
        this.sigle = sigle;
        this.titre = titre;
        this.description = description;
        this.duree = duree;
        this.contenu = contenu;
        this.ponderation = ponderation;
        this.epreuveFinale = epreuveFinale;
        this.idChampCompetences = idChampCompetences;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getPonderation() { return ponderation; }

    public void setPonderation(String ponderation) { this.ponderation = ponderation; }

    public String getEpreuveFinale() { return epreuveFinale; }

    public void setEpreuveFinale(String epreuveFinale) { this.epreuveFinale = epreuveFinale; }

    public int getIdChampCompetences() {
        return idChampCompetences;
    }

    public void setIdChampCompetences(int idChampCompetences) { this.idChampCompetences = idChampCompetences; }

    public Collection<CoursProfil> getCoursProfils() {
        return coursProfils;
    }

    public Collection<Competence> getCompetence() {
        return competence;
    }
}
