package com.example.e1563270.epreuvefinale.Network;

import com.example.e1563270.epreuvefinale.DAL.ChampCompetence;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiChampCompetence {
    @GET("champsCompetencesProgrammeParIdCours/{id}")
    Call<List<ChampCompetence>> getChampsCompetencesPourCours(@Path("id") int id);
}
