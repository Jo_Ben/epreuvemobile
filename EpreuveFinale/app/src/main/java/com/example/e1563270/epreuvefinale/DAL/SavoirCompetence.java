package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class SavoirCompetence {
    @SerializedName("Id")
    private int id;
    @SerializedName("Description")
    private String description;
    @SerializedName("Type")
    private int type;
    @SerializedName("No")
    private int no;
    @SerializedName("IdCompetence")
    private int idCompetence;

    public int getId() { return id; }
    public int getType() { return type; }
    public int getNo() { return no; }
    public int getIdCompetence() { return idCompetence; }
    public String getDescription() { return description; }

    public void setType(int type) { this.type = type; }
    public void setId(int id) { this.id = id; }
    public void setDescription(String description) { this.description = description; }
    public void setNo(int no) { this.no = no; }
    public void setIdCompetence(int idCompetence) { this.idCompetence = idCompetence; }

    public SavoirCompetence() { }

    public SavoirCompetence(int id, String description, int type, int no, int idCompetence) {
        this.id = id;
        this.description = description;
        this.type = type;
        this.no = no;
        this.idCompetence = idCompetence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavoirCompetence that = (SavoirCompetence) o;
        return id == that.id &&
                type == that.type &&
                no == that.no &&
                idCompetence == that.idCompetence &&
                Objects.equals(description, that.description);
    }
}

