package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

public class CoursProfil {

    @SerializedName("Id")
    private int id;
    @SerializedName("IdCours")
    private int idCours;
    @SerializedName("IdProfil")
    private int idProfil;
    @SerializedName("Session")
    private int session;
    @SerializedName("PositionVerticale")
    private int positionVerticale;

    private Cours cours;
    private Profil profil;

    public CoursProfil() {}

    public CoursProfil(int id, int idCours, int idProfil, int session, int positionVerticale) {
        this.id = id;
        this.idCours = idCours;
        this.idProfil = idProfil;
        this.session = session;
        this.positionVerticale = positionVerticale;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCours() {
        return idCours;
    }

    public void setIdCours(int idCours) {
        this.idCours = idCours;
    }

    public int getIdProfil() {
        return idProfil;
    }

    public void setIdProfil(int idProfil) {
        this.idProfil = idProfil;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getPositionVerticale() {
        return positionVerticale;
    }

    public void setPositionVerticale(int positionVerticale) { this.positionVerticale = positionVerticale; }
}
