package com.example.e1563270.epreuvefinale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.e1563270.epreuvefinale.Fragment.FormulaireFragment;

public class FormulaireActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulaire);

        FragmentRedirection();
    }

    @Override
    public void onBackPressed() {
        Intent homepage = new Intent(FormulaireActivity.this, MainActivity.class);
        startActivity(homepage);
    }

    private void FragmentRedirection() {
        Fragment fragment = new FormulaireFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }
}