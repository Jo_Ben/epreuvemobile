package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Profil {

    @SerializedName("Id")
    private int id;
    @SerializedName("Nom")
    private String nom;
    @SerializedName("Description")
    private String description;
    @SerializedName("Abbreviation")
    private String abbreviation;
    @SerializedName("IdProgramme")
    private int idProgramme;

    private Programme programme;

    public Profil(int id, String nom, String description, String abbreviation, int idProgramme) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.abbreviation = abbreviation;
        this.idProgramme = idProgramme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.nom = description;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public int getIdProgramme() {
        return idProgramme;
    }

    public void setIdProgramme(int idProgramme) {
        this.idProgramme = idProgramme;
    }
}