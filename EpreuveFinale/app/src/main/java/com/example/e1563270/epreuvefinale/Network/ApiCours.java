package com.example.e1563270.epreuvefinale.Network;

import com.example.e1563270.epreuvefinale.DAL.Cours;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCours {
    @GET("Cours")
    Call<List<Cours>> getCours();

    @GET("CoursParIdProfil/{id}")
    Call<List<Cours>> getCoursParIdProfils(@Path("id") int id);

    @GET("CoursParId/{id}")
    Call<Cours> getCoursParId(@Path("id") int id);

    @PUT("Cours/{id}")
    Call<Cours> putCours(@Path("id") int id, @Body Cours cours);
}
