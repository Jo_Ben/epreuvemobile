package com.example.e1563270.epreuvefinale.DAL;

import com.google.gson.annotations.SerializedName;

public class CriterePerformanceCompetence {
    @SerializedName("Id")
    private int id;
    @SerializedName("No")
    private int no;
    @SerializedName("Critere")
    private String critere;

    @SerializedName("IdCompetence")
    private int idCompetence;
    private Competence competence;

    public CriterePerformanceCompetence() {}

    public CriterePerformanceCompetence(int id, int no, String critere, int idCompetence) {
        this.id = id;
        this.no = no;
        this.critere = critere;
        this.idCompetence = idCompetence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getCritere() {
        return critere;
    }

    public void setCritere(String critere) {
        this.critere = critere;
    }

    public int getIdCompetence() {
        return idCompetence;
    }

    public void setICompetence(int idtCompetence) {
        this.idCompetence = idCompetence;
    }

    public Competence getCompetence() {
        return competence;
    }

    public void setCompetence(Competence competence) {
        this.competence = competence;
    }

    @Override
    public boolean equals(Object o) {
        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                CriterePerformanceCompetence c = (CriterePerformanceCompetence) o;
                egale = (id == c.id);
            }
        }
        return egale;

    }
    @Override
    public int hashCode() {
        return id;
    }
}

