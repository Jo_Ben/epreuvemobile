package com.example.e1563270.epreuvefinale.Network;

import com.example.e1563270.epreuvefinale.DAL.Cours;
import com.example.e1563270.epreuvefinale.DAL.Profil;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiProfil {
    @GET("Profils")
    Call<List<Profil>> getProfils();
}
