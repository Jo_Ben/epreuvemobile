package com.example.e1563270.epreuvefinale.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.e1563270.epreuvefinale.DAL.Cours;
import com.example.e1563270.epreuvefinale.R;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Cours> {
    private Context context;
    private List<Cours> cours;

    public ListViewAdapter(Context context, int resource, List<Cours> cours) {
        super(context, resource, cours);
        this.context = context;
        this.cours = cours;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listCompetence = convertView;

        if(listCompetence == null)
            listCompetence = LayoutInflater.from(this.context).inflate(R.layout.activity_listview,parent,false);

        Cours cour = cours.get(position);

        TextView code = listCompetence.findViewById(R.id.text1);
        code.setText(cour.getSigle());

        TextView enonce = listCompetence.findViewById(R.id.text2);
        enonce.setText(cour.getTitre());

        return listCompetence;
    }
}
